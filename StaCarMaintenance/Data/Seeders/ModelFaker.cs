using Bogus;
using Microsoft.EntityFrameworkCore;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Data.Seeders;

public class ModelFaker : Faker<Model.Model>
{
    public ModelFaker(DbSet<Brand> brands)
    {
        RuleFor(b => b.Name, f => f.Vehicle.Model());
        RuleFor(b => b.Brand, f => f.PickRandom<Brand>(brands));
    }
}