using Bogus;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Data.Seeders;

public class BrandFaker : Faker<Brand>
{
    public BrandFaker()
    {
        RuleFor(b => b.Name, f => f.Vehicle.Manufacturer());
        RuleFor(b => b.PriceApiUrl, f => f.Parse("https://localhost:5001/"));
        RuleFor(b => b.PriceApiKey, f => f.Parse(Guid.NewGuid().ToString()));
    }
}