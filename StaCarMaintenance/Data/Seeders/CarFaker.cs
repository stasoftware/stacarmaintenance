using Bogus;
using Bogus.Extensions.UnitedKingdom;
using Microsoft.EntityFrameworkCore;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Data.Seeders;

public class CarFaker : Faker<Car>
{
    public CarFaker(DbSet<Model.Model> models, DbSet<Customer> customers)
    {
        RuleFor(b => b.Vin, f => f.Vehicle.Vin());
        RuleFor(b => b.LicensePlate, f => f.Random.Replace("##-??-##"));
        RuleFor(b => b.Model, f => f.PickRandom<Model.Model>(models));
        RuleFor(b => b.Customer, f => f.PickRandom<Customer>(customers));
    }
}