using Bogus;
using Microsoft.EntityFrameworkCore;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Data.Seeders;

public class EngineerFaker : Faker<Engineer>
{
    public EngineerFaker()
    {
        RuleFor(b => b.Name, f => f.Parse("{{name.firstName}} {{name.lastName}}"));
    }
}