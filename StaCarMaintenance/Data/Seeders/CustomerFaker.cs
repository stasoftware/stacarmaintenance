using Bogus;
using Microsoft.EntityFrameworkCore;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Data.Seeders;

public class CustomerFaker : Faker<Customer>
{
    public CustomerFaker()
    {
        RuleFor(b => b.FirstName, f => f.Person.FirstName);
        RuleFor(b => b.LastName, f => f.Person.LastName);
        RuleFor(b => b.Email, f => f.Person.Email);
        RuleFor(b => b.Phone, f => f.Person.Phone);
    }
}