using Bogus;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using StaCarMaintenance.Data.Seeders;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Data;

public static class DataInitializer
{
    public static void Initialize(IServiceProvider serviceProvider)
    {
        using (var context = new DataContext(
                   serviceProvider.GetRequiredService<DbContextOptions<DataContext>>())
              )
        {
            string[] files =
            {
                "./Data/sta_car_maintenance.sqlite-shm",
                "./Data/sta_car_maintenance.sqlite-wal",
                "./Data/sta_car_maintenance.sqlite",
            };
            foreach (string file in files)
            {
                if(File.Exists(@file))
                {
                    File.Delete(@file);
                }
            }
            Console.Out.WriteLine("Removed the database files");

            context.Database.EnsureCreated();
            context.Database.Migrate();

            if (context.MaintenanceJobs.Any())
            {
                return;
            }

            //Testing
            for (int i = 0; i < 5; i++)
            {
                context.Brands.Add(
                    new BrandFaker().Generate()
                );
            }
            for (int i = 0; i < 10; i++)
            {
                context.Engineers.Add(
                    new EngineerFaker().Generate()
                );
            }
            for (int i = 0; i < 20; i++)
            {
                context.Customers.Add(
                    new CustomerFaker().Generate()
                );
            }

            int[] hourRatesRegular = { 75, 80, 85 };
            foreach (int rate in hourRatesRegular)
            {
                context.HourRates.Add(
                    new HourRates
                    {
                        Regular = rate,
                        Weekend = 1.5m*rate
                    }
                );
            }
            int[] vats = { 19, 21, 20 };
            foreach (int vat in vats)
            {
                context.Vats.Add(
                    new Vat()
                    {
                        Percentage = vat,
                    }
                );
            }
            
            List<TimeSlot> timeSlots = GenerateTimeSlots();
            context.TimeSlots.AddRange(timeSlots);
            context.SaveChanges();
            
            
            for (int i = 0; i < 10; i++)
            {
                context.Models.Add(
                    new ModelFaker(context.Brands).Generate()
                );
            }
            context.SaveChanges();

            
            for (int i = 0; i < 20; i++)
            {
                context.Cars.Add(
                    new CarFaker(context.Models, context.Customers).Generate()
                );
            }
            context.SaveChanges();

        }
    }

    private static List<TimeSlot> GenerateTimeSlots()
    {
        List<TimeSlot> timeSlots = new List<TimeSlot>();
        DateTime dateTime = DateTime.Now;
        DateTime endOfTheYear = new DateTime(dateTime.Year, 12,31);

        do{
            dateTime = dateTime.AddDays(1);
            DateTime startDateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 8, 0, 0);
            DateTime endDateTime = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 18, 0, 0);

            foreach (DateTime start in GenerateTimeRange(startDateTime, endDateTime))
            {
                timeSlots.Add(new TimeSlot
                {
                    Start = start
                }
                );
            }
        } while(dateTime < endOfTheYear);

        return timeSlots;
    }

    private static List<DateTime> GenerateTimeRange(DateTime startDateTime, DateTime endDateTime)
    {
        List<DateTime> dates = new List<DateTime>();
        for(DateTime start = startDateTime; start < endDateTime;start = start.AddHours(1)) {
            dates.Add(start);
        }
        return dates;
    }
}