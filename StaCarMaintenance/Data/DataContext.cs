using Microsoft.EntityFrameworkCore;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Data;

public class DataContext : DbContext
{
    public DbSet<Brand> Brands { get; set; }
    public DbSet<Model.Model> Models { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Car> Cars { get; set; }
    public DbSet<Engineer> Engineers { get; set; }
    public DbSet<HourRates> HourRates { get; set; }
    public DbSet<Vat> Vats { get; set; }
    public DbSet<MaintenanceJob> MaintenanceJobs { get; set; }
    public DbSet<SparePart> SpareParts { get; set; }
    public DbSet<TimeSlot> TimeSlots { get; set; }
    public DbSet<ScheduledMaintenanceJob> ScheduledMaintenanceJobs { get; set; }
    
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }

}