#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StaCarMaintenance.Data;
using StaCarMaintenance.Model;

namespace StaCarMaintenance.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduledMaintananceJobController : ControllerBase
    {
        private readonly DataContext _context;

        public ScheduledMaintananceJobController(DataContext context)
        {
            _context = context;
        }

        // GET: api/ScheduledMaintananceJob
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ScheduledMaintenanceJob>>> GetScheduledMaintenanceJobs()
        {
            return await _context.ScheduledMaintenanceJobs.ToListAsync();
        }

        // GET: api/ScheduledMaintananceJob/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ScheduledMaintenanceJob>> GetScheduledMaintenanceJob(int id)
        {
            var scheduledMaintenanceJob = await _context.ScheduledMaintenanceJobs.FindAsync(id);

            if (scheduledMaintenanceJob == null)
            {
                return NotFound();
            }

            return scheduledMaintenanceJob;
        }

        private bool ScheduledMaintenanceJobExists(int id)
        {
            return _context.ScheduledMaintenanceJobs.Any(e => e.Id == id);
        }
    }
}
