
# Sta Car Maintenance

### Context
A car maintenance company “Newton Car Maintenance” wants to register the maintenance jobs it
does on various cars, for various customers. Some maintenance jobs are specific for a certain brand of
car, or even a specific model of a car brand, others apply to all types of cars of all cars of a
brand/model. Each maintenance job has a fixed rate in terms of service hours and may require certain
spare parts. Spare parts can also be generic for (almost) all cars, others are specific per brand or even
per model. E.g., many cars share the same engine block.
A software architect made the following simplified class diagram. Properties and operations are
omitted.

![img.png](docs/class_diagram.png)

### Assignment
According to the class diagram, write some modern PHP-code that calculates the total
price for servicing a car (i.e., the price for an instance of ScheduledMainteanceJob). You
may not need to implement all of the classes in the diagram and you are allowed to add
extra classes if you think that’s necessary. Use object-oriented methodologies and use
some of the latest PHP features. Make sure your code runs on the latest version of PHP.
Your calculation should keep track of the following aspects:
* Maintenance servicing hours performed in the weekend costs more than maintenance during
  weekdays
* Spare parts have different costs based on the type of part
* Each type of maintenance job has a fixed rate in terms of service hours
* Sum up spare part prices and servicing hours
* Calculate VAT

Now, also write some code that gets the actual, current price of spare parts from an
external system at the car manufacturer. The price should be fetched in real-time and
used in the total servicing price calculation implemented above.
Make a list of all assumptions you made that were required for completing this
assignment.

Note: Use a reasonable level of comments in your code so others will be able to maintain
it.

## Start
Just run the program with the use of your IDE.
This will generate a sqlite database that will be seeded with fake data.


### Example Result (PHP - Terminal)
![img.png](docs/output-result.png)



## Installed 3rd party packages.

https://github.com/bchavez/Bogus



