using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class MaintenanceJob : AbstractModel
{

    [Required]
    public string Name { get; set; }

    public string Type { get; set; }

    public decimal CalculatedWorkingHours { get; set; }
    
    public HourRates HourRates  { get; set; }
    
    public virtual ICollection<SparePart> SpareParts { get; set; }
    
    public virtual ICollection<ScheduledMaintenanceJob> ScheduledMaintenanceJobs { get; set; }
}