using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class Car : AbstractModel
{
    [Required]
    public string Vin  { get; set; }
    
    public string LicensePlate  { get; set; }   
    
    public Customer Customer  { get; set; }
    public Model Model  { get; set; }
    
    public virtual ICollection<ScheduledMaintenanceJob> ScheduledMaintenanceJobs { get; set; }
}