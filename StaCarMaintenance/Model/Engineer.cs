using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class Engineer : AbstractModel
{

    [Required]
    public string Name { get; set; }
}