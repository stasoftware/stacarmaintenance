using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class Brand : AbstractModel
{

    [Required]
    public string Name  { get; set; }   
    
    
    public string? PriceApiUrl  { get; set; }   
    public string? PriceApiKey  { get; set; }   
    
    public virtual ICollection<SparePart>? SpareParts { get; set; }
    
    public virtual ICollection<Model>? Models { get; set; }
}