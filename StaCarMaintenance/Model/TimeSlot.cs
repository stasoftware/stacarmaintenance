using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class TimeSlot : AbstractModel
{
    [Required]
    public DateTime Start  { get; set; }
    
    public virtual ICollection<ScheduledMaintenanceJob> ScheduledMaintenanceJobs { get; set; }
}