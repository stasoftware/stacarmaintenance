using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class HourRates : AbstractModel
{
    public Decimal Regular { get; set; }
    public Decimal Weekend { get; set; }
    
    public virtual ICollection<MaintenanceJob> MaintenanceJobs { get; set; }
}