using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class SparePart : AbstractModel
{
    [Required]
    public string Name  { get; set; }   
    public string Ean  { get; set; }   
    public string PartNumber  { get; set; }   
    public decimal FixedPrice  { get; set; }   
    
    public virtual ICollection<Brand> Brands { get; set; }
    public virtual ICollection<Model> Models { get; set; }
}