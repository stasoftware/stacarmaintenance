using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class Model : AbstractModel
{
    [Required]
    public string Name  { get; set; }   
    
    public Brand Brand  { get; set; }   
    
    public virtual ICollection<SparePart> SpareParts { get; set; }
}