using System.ComponentModel.DataAnnotations;
namespace StaCarMaintenance.Model;

abstract public class AbstractModel
{
    [Key]
    public int Id { get; set; }
}