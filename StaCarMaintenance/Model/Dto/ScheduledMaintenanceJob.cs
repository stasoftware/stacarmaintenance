using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model.Dto;

public class ScheduledMaintenanceJobDto
{

    public decimal RegisteredWorkingHours { get; set; }

    public Engineer Engineer { get; set; }
    public TimeSlot TimeSlot { get; set; }
    public MaintenanceJob MaintenanceJob { get; set; }
    public Vat Vat { get; set; }
    
    public virtual ICollection<Car> Cars { get; set; }
}