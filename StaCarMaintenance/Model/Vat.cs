using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class Vat : AbstractModel
{
    [Required]
    public int Percentage { get; set; }
    
    public virtual ICollection<ScheduledMaintenanceJob> ScheduledMaintenanceJobs { get; set; }
}