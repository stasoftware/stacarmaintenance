using System.ComponentModel.DataAnnotations;

namespace StaCarMaintenance.Model;

public class Customer : AbstractModel
{

    [Required]
    public string FirstName { get; set; }
    public string LastName { get; set; }
    
    public string? Email { get; set; }
    
    public string? Phone { get; set; }
}